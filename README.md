# Attendance App Team 5

The application is used to normalize the data obtained by Microsoft Teams, and thus be in a single format for processing.

## Getting started

To run the tests you have to install the pytest package, for that it is advised to execute the following command:

```
pip install -r requirements.txt
```

### Run the app

To start the program you have to execute the main.py file, as shown below:
```
python main.py
```

## Team members

- Nelson Delgado Colque
- Rodrigo Morales Rivas
- Alex Perez Castillo