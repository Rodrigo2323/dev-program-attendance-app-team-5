import sys
import os
from attendance_app.src.logger_wrapper import setup_logger
from attendance_app.src.teams_files_reader import load_data

logger = setup_logger(__name__)


def main():
    logger.info("starting main")
    load_data()
    logger.info("finishing main")


if __name__ == '__main__':
    main()
