from attendance_app.src.attendance import Attendance
from attendance_app.src.duration import Duration
from attendance_app.src.summary import Summary


def build_attendance_object(raw):
    new_duration = Duration(raw['Duration'].get('hours'), raw['Duration'].get('minutes'),
                            raw['Duration'].get('seconds'))
    new_summary = Summary(raw.get('Title'), raw.get('Id'), raw.get('Attended participants'), raw.get('Start Time'),
                          raw.get('End Time'), new_duration)
    new_attendance = Attendance(raw.get('Start Time'), new_summary, raw.get('Participants'))
    return new_attendance
