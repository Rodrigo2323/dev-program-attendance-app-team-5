from attendance_app.src.duration_helper import *

def normalize_participant(raw: dict):
    result = {}
    for key, element in raw.items():
        if 'name' in key.lower():
            result['Name'] = element
        if 'join' in key.lower():
            result['First join'] = get_date_format_yy(element)
        if 'leave' in key.lower():
            result['Last leave'] = get_date_format_yy(element)
        if 'duration' in key.lower():
            result['In-meeting duration'] = element      
        if 'email' in key.lower():
            result['Email'] = element
            result['Participant ID (UPN)'] = element     
        if 'role' in key.lower():
            result['Role'] = element
    return result


def resolve_participants_v1(email, raw):
    participant = {
        'Name': '',
        'Join time': '',
        'Leave time': '',
        'Duration': {
            'hours': 0,
            'minutes': 0,
            'seconds': 0 
        },
        'Email': '',
        'Role': ''
    }
    for row in raw:
        if email == row.get('Email') :
            participant['Name'] = row.get('Name')
            join_time = get_date_format_yy(row.get('Join time'))
            leave_time = get_date_format_yy(row.get('Leave time'))
            participant['Join time'] = get_min_date(participant.get('Join time'), join_time)
            participant['Leave time'] = get_max_date(participant.get('Leave time'), leave_time)
            participant['Duration'] = sum_dict_durations(participant.get('Duration'), get_duration(join_time, leave_time))
            participant['Email'] = row.get('Email')
            participant['Role'] = row.get('Role')
    
    participant['Duration'] = dict_duration_to_str(participant['Duration'])
    return participant


def get_in_meeting_activities(raw, unique_key='Email'):
    participants = []
    if raw is None or len(raw) == 0:
        return participants
        
    list_emails = list(set([row.get(unique_key) for row in raw]))
    for email in list_emails:
        participant = resolve_participants_v1(email, raw)
        participants.append(participant)
    return participants


def resolve_participant_join_last_time(raw_data, unique_key='Email'):
    data = []
    no_email_participants = []
    for row in raw_data:
        if row.get(unique_key) is None:
            no_email_participants.append(row)
        else:
            data.append(row)

    in_meeting_activities = get_in_meeting_activities(data)
    in_meeting_activities.extend(no_email_participants)
    return in_meeting_activities

def normalize_in_meeting(raw: dict):
    result = {}
    for key, element in raw.items():
        if 'name' in key.lower():
            result['Name'] = element
        if 'join' in key.lower():
            result['Join time'] = get_date_format_yy(element)
        if 'leave' in key.lower():
            result['Leave time'] = get_date_format_yy(element)
        if 'duration' in key.lower():
            result['Duration'] = element      
        if 'email' in key.lower():
            result['Email'] = element    
        if 'role' in key.lower():
            result['Role'] = element
    return result

def build_participant_object(raw: dict):
    pass
