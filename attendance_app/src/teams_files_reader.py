from os import listdir
from os.path import isfile, join
import csv
import sys
from datetime import datetime
import os

from attendance_app.src.logger_wrapper import setup_logger
from attendance_app.src.summary_builder import normalize_summary_v2
from attendance_app.src.report_section_helper import *
from attendance_app.src.participant_builder import *

MEETING_SUMMARY = 'Meeting Summary'
FULL_NAME = 'Full Name'
PARTICIPANTS = '2. Participants'
ACTIVITIES = '3. In-Meeting activities'
logger = setup_logger(__name__)


def file_meet_conditions(path, pattern, file_path):
    return isfile(join(path, file_path)) and file_path.endswith(pattern)


# discovery de "data" files
def discover_data_files(path='attendance_app/data', pattern='csv'):
    return [join(path, f) for f in listdir(path) if file_meet_conditions(path, pattern, f)]


def get_file_data(path):
    with open(path, encoding='UTF-16') as reader:
        return list(csv.reader(reader, delimiter='\t'))


def get_summary_as_dict(summary):
    # convert list that contains "summary" lines into dict
    summary_dict = {}
    for element in summary:
        _, row = element
        # TODO must guarante index 0 and 1 exist
        summary_dict[row[0]] = row[1]
    return summary_dict


def get_data_as_dict(data):
    if data == []:
        return data

    result = []
    _, header = data[0]  # TODO must guarante index 0 exists
    for _, row in data:
        result.append(dict(zip(header, row)))
    return result


def normalize_raw_data(data, file_name, new_folder_path):
    is_old_file_version = [MEETING_SUMMARY] in data
    summary = extract_section_rows(data, MEETING_SUMMARY, FULL_NAME) if is_old_file_version else extract_section_rows(
        data, '1. Summary', PARTICIPANTS)
    participants = [] if is_old_file_version else extract_section_rows(
        data, PARTICIPANTS, ACTIVITIES)
    in_meetings = extract_section_rows(data, FULL_NAME,
                                       include_start=True) if is_old_file_version else extract_section_rows(data,
                                                                                                            ACTIVITIES)
    try:
        summary = get_summary_as_dict(summary)
        participants = get_data_as_dict(participants)
        in_meetings = get_data_as_dict(in_meetings)
        summary = normalize_summary_v2(summary)

        if len(participants) > 0:
            participants.pop(0)

        if len(in_meetings) > 1:
            in_meetings.pop(0)

        # in-meeting
        if is_old_file_version:
            in_meetings_activities = []
            for row in in_meetings:
                in_meetings_activities.append(normalize_in_meeting(row))
            in_meetings = in_meetings_activities

        # Participants:
        if is_old_file_version:
            in_meetings = resolve_participant_join_last_time(in_meetings)
            participants = []
            for row in in_meetings:
                participants.append(normalize_participant(row))

        create_csv(summary, participants, in_meetings, file_name, new_folder_path)
        # create_text_success(len(participants), file_name, new_folder_path)
        return participants
    except Exception as e:
        logger.exception(f"error trying to normalize the data in {file_name}, error type: {e}")


def create_text_success(participants, file_name, new_folder_path):
    new_path = new_folder_path + file_name
    with open((f'{new_folder_path}/success.txt'), 'a') as txt_file:
        if (participants and new_folder_path) is None:
            txt_file.write(" ")
        else:
            txt_file.write(str(participants) + ", ")
            txt_file.write(new_path + "\n")
    txt_file.close()


def create_text_error(file_name, new_folder_path):
    new_path = new_folder_path + file_name
    with open((f'{new_folder_path}/error.txt'), 'a') as txt_file:
        if file_name == "":
            txt_file.write("0")
        else:
            txt_file.write(new_path + "\n")
    txt_file.close()


def create_csv(data, participants, in_meetings, file_name, new_folder_path):
    new_path = new_folder_path + '/' + file_name
    try:
        with open(new_path, 'w') as csv_file:
            writer = csv.writer(csv_file, delimiter='\t')
            writer.writerow(['1. Summary'])
            for key, value in data.items():
                writer.writerow([key, value])
            writer.writerow([])
            writer.writerow(['2. Participants'])
            writer.writerow(
                ['Name', 'First join', 'Last leave', 'In-meeting duration', 'Email', 'Participant ID (UPN)', 'Role'])
            if len(participants) > 0:
                for row in participants:
                    writer.writerow(
                        [row.get('Name'), row.get('First join'), row.get('Last leave'), row.get('In-meeting duration'),
                         row.get('Email'), row.get('Participant ID (UPN)'), row.get('Role')])
            writer.writerow([])
            writer.writerow(['3. In-Meeting activities'])
            writer.writerow(['Name', 'Join time', 'Leave time',
                             'Duration', 'Email', 'Role'])
            if len(in_meetings) > 0:
                for row in in_meetings:
                    writer.writerow([row.get('Name'), row.get('Join time'), row.get('Leave time'), row.get('Duration'),
                                     row.get('Email'), row.get('Role')])
    except Exception as e:
        logger.exception(f"Error are trying to create the csv file, error type: {e}")


def create_folder(full_path):
    os.makedirs(full_path, exist_ok=True)


def create_path():
    date_now = datetime.now()
    date_now_string = date_now.strftime('%Y%m%d%I%M%S')
    folder_path = 'attendance_app/stage_data/'
    full_path = folder_path + date_now_string
    return full_path


def only_file_name(path_name):
    list_name = path_name.split('/')[-1].split('data')[-1]
    return list_name


def obtain_folder_path():
    full_path = create_path()
    create_folder(full_path)
    return full_path


def load_data():
    file_paths = discover_data_files() or []
    new_folder_path = obtain_folder_path()
    if file_paths == []:
        logger.exception(f"the folder attendance/data. is empty")
        return
    error = []
    for fp in file_paths:
        data = get_file_data(fp)
        name_file = only_file_name(fp)
        logger.info("reading file " + name_file)
        try:
            participant = normalize_raw_data(data, name_file, new_folder_path)
            create_text_success(len(participant), name_file, new_folder_path)
        except Exception as e:
            create_text_error(name_file, new_folder_path)
            error.append(name_file)
            logger.exception(f"Error in {name_file}, type error is: {e}")
    if error == []:
        create_text_error("", new_folder_path)
