from datetime import datetime, timedelta

DATE_MMDDYY = "%m/%d/%y, %I:%M:%S %p"
DATE_MMDDYYYY = "%m/%d/%Y, %I:%M:%S %p"


def get_elapsed_duration(start_date, end_date):
    if start_date is None or start_date == '' or end_date is None or end_date == '':
        return None

    start_dt = datetime.strptime(start_date, DATE_MMDDYY)
    end_dt = datetime.strptime(end_date, DATE_MMDDYY)
    elapsed = end_dt - start_dt

    if elapsed.days < 0:
        return None
    return elapsed


def get_dict_duration(data: datetime):
    if data is None or data == '':
        return None
    seconds = data.seconds
    minutes = int(seconds / 60)
    hours = int(minutes / 60)
    return {
        'hours': hours + (data.days * 24),
        'minutes': minutes % 60,
        'seconds': seconds - (hours * 3600) - (minutes % 60 * 60)
    }


def get_date_format_yy(date_text):
    result = ''
    if len(date_text) >= 22:
        result = str(datetime.strptime(date_text, DATE_MMDDYYYY).strftime(DATE_MMDDYY)).split()
    else:
        result = str(datetime.strptime(date_text, DATE_MMDDYY).strftime(DATE_MMDDYY)).split()
    return f"{result[0].lstrip('0')} {result[1].lstrip('0')} {result[2].lstrip('0')}"


def get_duration(start_date, end_date):
    start_dt = get_date_format_yy(start_date)
    end_dt = get_date_format_yy(end_date)
    elapsed = get_elapsed_duration(start_dt, end_dt)
    return get_dict_duration(elapsed)


def get_max_date(first_date, second_date):
    if first_date is None or first_date == '':
        return second_date

    ft_date = datetime.strptime(first_date, DATE_MMDDYY)
    sd_date = datetime.strptime(second_date, DATE_MMDDYY)

    if sd_date > ft_date:
        return second_date
    return first_date


def get_min_date(first_date, second_date):
    if first_date is None or first_date == '':
        return second_date

    ft_date = datetime.strptime(first_date, DATE_MMDDYY)
    sd_date = datetime.strptime(second_date, DATE_MMDDYY)

    if sd_date < ft_date:
        return second_date
    return first_date


def sum_dict_durations(dir_1: dict, dir_2: dict):
    seconds = dir_1['seconds'] + dir_2['seconds']
    minutes = dir_1['minutes'] + dir_2['minutes']
    hours = dir_1['hours'] + dir_2['hours']
    if seconds >= 60:
        seconds = seconds - 60
        minutes = minutes + 1
    if minutes >= 60:
        minutes = minutes - 60
        hours = hours + 1
    return {
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    }


def dict_duration_to_str(duration: dict):
    result = ''
    if duration.get('hours') > 0:
        result += f"{duration.get('hours')}h "
    if duration.get('minutes') > 0:
        result += f"{duration.get('minutes')}m "
    if duration.get('seconds') > 0:
        result += f"{duration.get('seconds')}s"
    return result


def convert_duration_str_to_dict(duration_str):
    return dict()


def convert_dict_to_object():
    pass
