from datetime import datetime
from attendance_app.src.duration import Duration
from attendance_app.src.duration_helper import dict_duration_to_str
from attendance_app.src.summary import Summary

DATE_FORMAT = '%m/%d/%Y, %I:%M:%S %p'
DATE_FORMAT_2 = '%m/%d/%y, %I:%M:%S %p'
START_TIME_OPTIONS = ['Meeting Start Time', 'Start time', 'First Join Time']
ATTENDEE_OPTIONS = ['Total Number of Participants']
END_TIME_OPTIONS = ['Meeting End Time', 'End time', 'Last Leave Time']
TITLE_OPTIONS = ['Title', 'Meeting title']


def normalize_summary(raw_data: dict):
    duration = obtain_duration(raw_data)
    new_time = dict_duration_to_str(duration)
    summary_dict = {
        'Meeting Title': obtain_data(raw_data, 'Meeting Title', TITLE_OPTIONS),
        'Attended participants': int(obtain_data(raw_data, 'Attended participants', ATTENDEE_OPTIONS)),
        'Start Time': obtain_data(raw_data, 'Start Time', START_TIME_OPTIONS),
        'End Time': obtain_data(raw_data, 'End Time', END_TIME_OPTIONS),
        'Duration': duration,
        'Id': id_problem('Id', raw_data)
    }
    return summary_dict


def normalize_summary_v2(raw_data: dict):
    duration = obtain_duration(raw_data)
    new_time = dict_duration_to_str(duration)
    summary_dict = {
        'Meeting Title': obtain_data(raw_data, 'Meeting Title', TITLE_OPTIONS),
        'Attended participants': int(obtain_data(raw_data, 'Attended participants', ATTENDEE_OPTIONS)),
        'Start Time': obtain_data(raw_data, 'Start Time', START_TIME_OPTIONS),
        'End Time': obtain_data(raw_data, 'End Time', END_TIME_OPTIONS),
        'Duration': new_time,
        'Id': id_problem('Id', raw_data)
    }
    return summary_dict


def obtain_data(data, option1, option_list):
    if option1 in data:
        return data.get(option1)
    for option in option_list:
        if option in data:
            return data.get(option)


def obtain_duration(data):
    start_time = obtain_data(data, 'Start Time', START_TIME_OPTIONS)
    end_time = obtain_data(data, 'End Time', END_TIME_OPTIONS)
    if 'Meeting Start Time' in data:
        new_time = diff_time(start_time, end_time, DATE_FORMAT)
        return new_time
    new_time = diff_time(start_time, end_time, DATE_FORMAT_2)
    return new_time


def diff_time(start, end, format_date):
    start_time = datetime.strptime(start, format_date)
    end_time = datetime.strptime(end, format_date)
    duration = end_time - start_time
    hour, extra_data = divmod(duration.seconds, 3600)
    minutes, seconds = divmod(extra_data, 60)
    return {'hours': hour, 'minutes': minutes, 'seconds': seconds}


def id_problem(key, raw_data):
    if key in raw_data:
        expected_value = raw_data.get(key)
        return expected_value
    elif 'Meeting Id' in raw_data:
        expected_value = raw_data.get('Meeting Id')
        return expected_value
    return obtain_data(raw_data, 'Meeting Title', TITLE_OPTIONS)


def build_summary_object(raw_data: dict):
    new_dict = normalize_summary(raw_data)
    duration_format = new_dict.get('Duration')
    duration = Duration(duration_format.get('hours'), duration_format.get('minutes'), duration_format.get('seconds'))
    new_summary = Summary(
        new_dict.get('Meeting Title'),
        new_dict.get('Id'),
        new_dict.get('Attended participants'),
        new_dict.get('Start Time'),
        new_dict.get('End Time'),
        duration
    )
    return new_summary
